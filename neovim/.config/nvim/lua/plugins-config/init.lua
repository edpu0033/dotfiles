-- plugins config
require('plugins-config.nvim-tree-config')
require('plugins-config.nvim-lspconfig')
require('plugins-config.nvim-cmp-config')
-- require('plugns-config.tabnine-config')
require('plugins-config.lualine-config')
require('plugins-config.barbar-config')
require('plugins-config.nvim-treesitter-config')
require('plugins-config.indent-blankline-config')
require('plugins-config.lua-ls')
require('plugins-config.lsp-format-config')
require('plugins-config.telekasten-config')

-- luasnip setup
local luasnip = require 'luasnip'
